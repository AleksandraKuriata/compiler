RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
all:
	$(MAKE) parser
	$(MAKE) lex
	$(MAKE) compile
	g++ -o compiler/compiler compiler/main.c compiler/lex.yy.c compiler/parser.tab.c compiler/definition.o compiler/expressions.o compiler/src/activity.o compiler/src/conditions.o -lfl -lstdc++
	sudo mv compiler/compiler bin
	echo ${GREEN}Compilation finished!${NC}
	# g++ -std=c++11 -Wall -Wextra compiler/main.cpp compiler/lexgram.a
parser:
	bison -d compiler/parser.y
	sudo mv parser.tab.* compiler

lex:
	flex compiler/lex.l
	sudo mv lex.yy.* compiler

compile: 
	g++ -c compiler/definition.c -o definition.o -lstdc++ -lfl
	sudo mv definition.o compiler
	g++ -c compiler/expressions.c -o expressions.o -lstdc++ -lfl
	sudo mv expressions.o compiler
	g++ -c compiler/src/activity.c -o activity.o -lstdc++ -lfl
	sudo mv activity.o compiler/src
	g++ -c compiler/src/conditions.c -o conditions.o -lstdc++ -lfl
	sudo mv conditions.o compiler/src
# compilerun:
	# $(MAKE) all
	# ./bin/a.exe < test/enter.code

 run:
	 ./bin/compiler  jftt2017-testy/program0.imp
	 ./bin/compiler  jftt2017-testy/program1.imp
	 ./bin/compiler  jftt2017-testy/program2.imp
	 ./bin/compiler  jftt2017-testy/1-numbers.imp
	 ./bin/compiler  jftt2017-testy/2-fib.imp
	 ./bin/compiler  jftt2017-testy/3-fib-factorial.imp
	 ./bin/compiler  jftt2017-testy/4-factorial.imp
	 ./bin/compiler  jftt2017-testy/5-tab.imp
	 ./bin/compiler  jftt2017-testy/6-mod-mult.imp
	 ./bin/compiler  jftt2017-testy/7-loopiii.imp
	 ./bin/compiler  jftt2017-testy/8-for.imp
	 ./bin/compiler  jftt2017-testy/9-sort.imp
	 
runmr:
	 ./interpreter/interpreter  jftt2017-testy/program0.mr
	 ./interpreter/interpreter  jftt2017-testy/program1.mr
	 ./interpreter/interpreter  jftt2017-testy/program2.mr
	 ./interpreter/interpreter  jftt2017-testy/1-numbers.mr
	 ./interpreter/interpreter  jftt2017-testy/2-fib.mr
	 ./interpreter/interpreter  jftt2017-testy/3-fib-factorial.mr
	 ./interpreter/interpreter  jftt2017-testy/4-factorial.mr
	 ./interpreter/interpreter  jftt2017-testy/5-tab.mr
	 ./interpreter/interpreter  jftt2017-testy/6-mod-mult.imp
	 ./interpreter/interpreter  jftt2017-testy/7-loopiii.imp
	 	 ./interpreter/interpreter  jftt2017-testy/8-for.mr
	 ./interpreter/interpreter  jftt2017-testy/9-sort.mr