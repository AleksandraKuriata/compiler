%option noyywrap
%option yylineno

%{
    #include <string.h>

    #include "parser.tab.h"

    int yylex();
%}

IDENTIFIER [_a-z]+
NUMBER [0-9]+

%x comment

%%
"("           BEGIN(comment);
<comment>")"  BEGIN(INITIAL);
<comment>. ;

[ \t\r\n]+	{} ;

{IDENTIFIER}	{ yylval.text = (char *)strdup(yytext); return PIDENTIFIER; }
{NUMBER}	    { yylval.num = (char *)strdup(yytext); return NUM;          }

VAR	        {   return(VAR);            }
BEGIN	    {   return(PROGRAM_BEGIN);  }
END         {   return(END);            }

IF	        {   return(IF);         }
ELSE	    {   return(ELSE);       }
THEN	    {   return(THEN);       }
ENDIF	    {   return(ENDIF);      }

WHILE	    {   return(WHILE);      }
DO	        {   return(DO);         }
ENDWHILE    {    return(ENDWHILE);  }

FOR	        {   return(FOR);        }
FROM	    {   return(FROM);       }
TO	        {   return(TO);         }
ENDFOR	    {   return(ENDFOR);     }
DOWNTO	    {   return(DOWNTO);     }

READ	    {   return(READ);       }
WRITE	    {   return(WRITE);      }


"["	        { return(ARRAY_LBRACKET);   }
"]"	        { return(ARRAY_RBRACKET);   }

"+"	        { return(ADD);      }
"-"	        { return(SUB);      }
"*"	        { return(MULT);    }
"/"	        { return(DIV);      }
"%"	        { return(MOD);      }

"="	        { return(OPERATOR_EQ);  }
"<>"	    { return(OPERATOR_NE);  }
"<"	        { return(OPERATOR_LT);  }
">"	        { return(OPERATOR_GT);  }
"<="	    { return(OPERATOR_LE);  }
">="	    { return(OPERATOR_GE);  }

":="	    { return(SET);          }
";"     	{ return(SEMICOLON);    }	
.	        {                       }

%%

