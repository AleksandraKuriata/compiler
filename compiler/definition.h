#ifndef DEFINITION_H_
#define DEFINITION_H_

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string.h>
#include <iostream>
#include <string>

//---------------STRUCTURES DEFINITIONS-------------
 struct declaredVariable {
	char * identifier;
	int address;
	int isArrayOfVariables;
	int isLoopTemporaryVariable;
	int isInitializedInLoop;
	int size;
};

//---------------TYPE DEFINITIONS-------------
extern unsigned int variableFreeAddress;
extern std::vector<declaredVariable*> declaredVariables;
extern std::vector<std::string> interpreterCode;

//---------------FUNCTIONS--------------------
int genericInitialize(char *identifier,int size, int isArrayOfVariables, int isInitializedInLoop);
int initializeVariable(char *identifier);
int initializeArrayOfVariables(char *identifier, int size);
int initializeLoopVariable(char * identifier);
int getPositionOfVariable(const char * identifier);
int removeVariable(const char * identifier);

#endif
