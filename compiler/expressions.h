#ifndef EXPRESSIONS_H_
#define EXPRESSIONS_H_

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string.h>
#include <iostream>
#include <string>

//---------------TYPE DEFINITIONS-------------
extern std::vector<std::string> interpreterCode;
extern char *instructionBuffer;
extern unsigned int freeAddressPointer;

//---------------FUNCTIONS--------------------
void createUserInputValue(long long value);

//--STACK FUNCTIONS--
void pushDataOnStack(const char * instruction);
void pushParameterDataOnStack(const char * instruction,int arg);

#endif
