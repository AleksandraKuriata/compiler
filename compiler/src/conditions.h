#ifndef CONDITIONS_H_
#define CONDITIONS_H_

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string.h>
#include <iostream>
#include <string>

//--CONDITIONS--
void conditionEqual();
void conditionNotEqual();
void conditionLowerThen();
void conditionGreaterThen();
void conditionLowerEqualThen();
void conditionGreaterEqualThen();

#endif
