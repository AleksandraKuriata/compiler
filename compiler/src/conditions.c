#include "../expressions.h"


void conditionNotEqual() {
	pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	pushParameterDataOnStack("SUB", freeAddressPointer - 1);
	 int currentLine = interpreterCode.size();
	pushParameterDataOnStack("JZERO", currentLine + 2);
	pushParameterDataOnStack("JUMP", currentLine + 4);
	pushParameterDataOnStack("LOAD", freeAddressPointer - 1);
	pushParameterDataOnStack("SUB", freeAddressPointer - 2);

	pushDataOnStack("space");

}
void conditionLowerThen() {
	pushParameterDataOnStack("LOAD", freeAddressPointer - 1);
	pushParameterDataOnStack("SUB", freeAddressPointer - 2);

	pushDataOnStack("space");

}
void conditionGreaterThen() {
	pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	pushParameterDataOnStack("SUB", freeAddressPointer - 1);

	pushDataOnStack("space");

}

void conditionLowerEqualThen() {
	pushParameterDataOnStack("LOAD", freeAddressPointer - 1);
	pushDataOnStack("INC");
	pushParameterDataOnStack("SUB", freeAddressPointer - 2);

	pushDataOnStack("space");

}

void conditionGreaterEqualThen() {
	pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	pushDataOnStack("INC");
	pushParameterDataOnStack("SUB", freeAddressPointer - 1);

	pushDataOnStack("space");
}

void conditionEqual() {
	pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	pushDataOnStack("INC");
	pushParameterDataOnStack("SUB", freeAddressPointer - 1);
	int currentLine = interpreterCode.size();
	pushParameterDataOnStack("JZERO", currentLine + 4);
	pushParameterDataOnStack("LOAD", freeAddressPointer - 1);
	pushDataOnStack("INC");
	pushParameterDataOnStack("SUB", freeAddressPointer - 2);

	pushDataOnStack("space");

}