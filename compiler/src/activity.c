#include "../expressions.h"

void addition() {
	pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	pushParameterDataOnStack("ADD", freeAddressPointer - 1);
	// pushParameterDataOnStack("STORE", freeAddressPointer - 2);
}
void subtraction() {
	pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	pushParameterDataOnStack("SUB", freeAddressPointer - 1);
	//pushParameterDataOnStack("STORE", freeAddressPointer - 2);
}
void multiplication() {

	int pointForMultiplication = interpreterCode.size();
	pushDataOnStack("ZERO");
	pushParameterDataOnStack("STORE", freeAddressPointer);
	pushParameterDataOnStack("LOAD", freeAddressPointer - 2);

	pushParameterDataOnStack("JZERO", pointForMultiplication + 16);
	pushParameterDataOnStack("JODD", pointForMultiplication + 6);
	pushParameterDataOnStack("JUMP", pointForMultiplication + 9);
	pushParameterDataOnStack("LOAD", freeAddressPointer);
	pushParameterDataOnStack("ADD", freeAddressPointer - 1);
	pushParameterDataOnStack("STORE", freeAddressPointer);
	pushParameterDataOnStack("LOAD", freeAddressPointer - 1);
	pushDataOnStack("SHL");
	pushParameterDataOnStack("STORE", freeAddressPointer - 1);
	// pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	// pushDataOnStack("SHL");
	pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	pushDataOnStack("SHR");
	pushParameterDataOnStack("STORE", freeAddressPointer - 2);
	pushParameterDataOnStack("JUMP", pointForMultiplication+3);


	pushParameterDataOnStack("LOAD", freeAddressPointer);
	 pushParameterDataOnStack("STORE", freeAddressPointer - 1);

	// pushDataOnStack("ZERO");
	// pushParameterDataOnStack("STORE", freeAddressPointer-1);
	// int currentLine = interpreterCode.size();
	// pushParameterDataOnStack("JUMP", 5+currentLine);
	// pushParameterDataOnStack("LOAD", freeAddressPointer-1);
	// pushParameterDataOnStack("ADD", freeAddressPointer - 3);
	// pushParameterDataOnStack("STORE", freeAddressPointer-1);
	// pushParameterDataOnStack("JUMP", 7+currentLine);
	// pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	// pushParameterDataOnStack("JODD", 1+currentLine);
	// pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	// pushDataOnStack("SHR");
	// pushParameterDataOnStack("STORE", freeAddressPointer - 2);
	// pushParameterDataOnStack("LOAD", freeAddressPointer - 3);
	// pushDataOnStack("SHL");
	// pushParameterDataOnStack("STORE", freeAddressPointer - 3);
	// pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	// pushParameterDataOnStack("JZERO", 16+currentLine);
	// pushParameterDataOnStack("JUMP", 5+currentLine);

	//  << "ZERO\n"
    //         << "STORE 2\n"
    //         << get_jump(7, offset)    // goto @main
    //         << "LOAD 2\n"            // jedynka
    //         << "ADD 0\n"
    //         << "STORE 2\n"
    //         << get_jump(9, offset)    // goto @step
    //         << "LOAD 1\n"            // main
    //         << get_jodd(3, offset)    // goto @jedynka
    //         << "LOAD 1\n"            // step
    //         << "SHR\n"
    //         << "STORE 1\n"
    //         << "LOAD 0\n"
    //         << "SHL\n"
    //         << "STORE 0\n"
    //         << "LOAD 1\n"
}
void division() {
	pushParameterDataOnStack("LOAD", freeAddressPointer - 1);
	int beforeDivOperation = interpreterCode.size();
	pushParameterDataOnStack("JZERO", beforeDivOperation + 52);

	pushParameterDataOnStack("LOAD", freeAddressPointer - 1);
	pushParameterDataOnStack("STORE", freeAddressPointer);
	pushDataOnStack("ZERO");
	pushDataOnStack("INC");
	pushParameterDataOnStack("STORE", freeAddressPointer + 1);
	pushDataOnStack("DEC");
	pushDataOnStack("SHR");
	pushParameterDataOnStack("LOAD", freeAddressPointer + 1);
	int firstLoopBegin = interpreterCode.size();
	pushParameterDataOnStack("JZERO", firstLoopBegin + 11);
	pushParameterDataOnStack("LOAD", freeAddressPointer);
	pushDataOnStack("SHL");
	pushParameterDataOnStack("STORE", freeAddressPointer);
	pushParameterDataOnStack("LOAD", freeAddressPointer + 1);
	pushDataOnStack("SHL");
	pushParameterDataOnStack("STORE", freeAddressPointer + 1);
	pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	pushDataOnStack("INC");
	pushParameterDataOnStack("SUB", freeAddressPointer);
	pushParameterDataOnStack("JUMP", firstLoopBegin);

	pushParameterDataOnStack("LOAD", freeAddressPointer);
	pushDataOnStack("SHR");
	pushParameterDataOnStack("STORE", freeAddressPointer);
	pushParameterDataOnStack("LOAD", freeAddressPointer + 1);
	pushDataOnStack("SHR");
	pushParameterDataOnStack("STORE", freeAddressPointer + 1);

	pushDataOnStack("ZERO");
	pushParameterDataOnStack("STORE", freeAddressPointer + 2);
	int secondLoopBegin = interpreterCode.size();
	pushParameterDataOnStack("LOAD", freeAddressPointer);
	pushDataOnStack("INC");
	pushParameterDataOnStack("SUB", freeAddressPointer - 1);
	pushParameterDataOnStack("JZERO", secondLoopBegin + 23);
	pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	pushDataOnStack("INC");
	pushParameterDataOnStack("SUB", freeAddressPointer);
	int secondLoopIfBegin = interpreterCode.size();
	pushParameterDataOnStack("JZERO", secondLoopIfBegin + 7);
	pushParameterDataOnStack("LOAD", freeAddressPointer + 2);
	pushParameterDataOnStack("ADD", freeAddressPointer + 1);
	pushParameterDataOnStack("STORE", freeAddressPointer + 2);
	pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
	pushParameterDataOnStack("SUB", freeAddressPointer);
	pushParameterDataOnStack("STORE", freeAddressPointer - 2);
	pushParameterDataOnStack("LOAD", freeAddressPointer + 1);
	pushDataOnStack("SHR");
	pushParameterDataOnStack("STORE", freeAddressPointer + 1);
	pushDataOnStack("SHR");
	pushDataOnStack("INC");
	pushParameterDataOnStack("LOAD", freeAddressPointer);
	pushDataOnStack("SHR");
	pushParameterDataOnStack("STORE", freeAddressPointer);
	pushParameterDataOnStack("JUMP", secondLoopBegin);

	pushParameterDataOnStack("LOAD", freeAddressPointer + 2);
	pushParameterDataOnStack("STORE", freeAddressPointer - 2);

	int afterOperation = interpreterCode.size();
	pushParameterDataOnStack("JUMP", afterOperation + 3);
	pushParameterDataOnStack("LOAD", freeAddressPointer - 1);
	pushParameterDataOnStack("STORE", freeAddressPointer - 2);
}



void modulo(char * instruction) {

	int secondPassedParameter = atoi( instruction );
	
	if(secondPassedParameter == 0 && isdigit(instruction[0])){
		pushDataOnStack("ZERO");
	}
	else if(secondPassedParameter == 2){
		int curr = interpreterCode.size();
		pushParameterDataOnStack("LOADI", freeAddressPointer);
		pushParameterDataOnStack("JODD", curr+4);
		pushDataOnStack("ZERO");
		pushParameterDataOnStack("JUMP", curr+6);
		pushDataOnStack("ZERO");
		pushDataOnStack("INC");
		return;
	}
	else{
		pushParameterDataOnStack("LOAD", freeAddressPointer - 1);
		int beforeDivOperation = interpreterCode.size();
		pushParameterDataOnStack("JZERO", beforeDivOperation + 52);

		pushParameterDataOnStack("LOAD", freeAddressPointer - 1);
		pushParameterDataOnStack("STORE", freeAddressPointer);
		pushDataOnStack("ZERO");
		pushDataOnStack("INC");
		pushParameterDataOnStack("STORE", freeAddressPointer + 1);
		pushDataOnStack("DEC");
		pushDataOnStack("SHR");
		pushParameterDataOnStack("LOAD", freeAddressPointer + 1);
		int firstLoopBegin = interpreterCode.size();
		pushParameterDataOnStack("JZERO", firstLoopBegin + 11);
		pushParameterDataOnStack("LOAD", freeAddressPointer);
		pushDataOnStack("SHL");
		pushParameterDataOnStack("STORE", freeAddressPointer);
		pushParameterDataOnStack("LOAD", freeAddressPointer + 1);
		pushDataOnStack("SHL");
		pushParameterDataOnStack("STORE", freeAddressPointer + 1);
		pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
		pushDataOnStack("INC");
		pushParameterDataOnStack("SUB", freeAddressPointer);
		pushParameterDataOnStack("JUMP", firstLoopBegin);

		pushParameterDataOnStack("LOAD", freeAddressPointer);
		pushDataOnStack("SHR");
		pushParameterDataOnStack("STORE", freeAddressPointer);
		pushParameterDataOnStack("LOAD", freeAddressPointer + 1);
		pushDataOnStack("SHR");
		pushParameterDataOnStack("STORE", freeAddressPointer + 1);

		pushDataOnStack("ZERO");
		pushParameterDataOnStack("STORE", freeAddressPointer + 2);
		int secondLoopBegin = interpreterCode.size();
		pushParameterDataOnStack("LOAD", freeAddressPointer);
		pushDataOnStack("INC");
		pushParameterDataOnStack("SUB", freeAddressPointer - 1);
		pushParameterDataOnStack("JZERO", secondLoopBegin + 23);
		pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
		pushDataOnStack("INC");
		pushParameterDataOnStack("SUB", freeAddressPointer);
		int secondLoopIfBegin = interpreterCode.size();
		pushParameterDataOnStack("JZERO", secondLoopIfBegin + 7);
		pushParameterDataOnStack("LOAD", freeAddressPointer + 2);
		pushParameterDataOnStack("ADD", freeAddressPointer + 1);
		pushParameterDataOnStack("STORE", freeAddressPointer + 2);
		pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
		pushParameterDataOnStack("SUB", freeAddressPointer);
		pushParameterDataOnStack("STORE", freeAddressPointer - 2);
		pushParameterDataOnStack("LOAD", freeAddressPointer + 1);
		pushDataOnStack("SHR");
		pushParameterDataOnStack("STORE", freeAddressPointer + 1);
		pushDataOnStack("SHR");
		pushDataOnStack("INC");
		pushParameterDataOnStack("LOAD", freeAddressPointer);
		pushDataOnStack("SHR");
		pushParameterDataOnStack("STORE", freeAddressPointer);
		pushParameterDataOnStack("JUMP", secondLoopBegin);

		pushParameterDataOnStack("LOAD", freeAddressPointer - 2);
		pushParameterDataOnStack("STORE", freeAddressPointer - 2);
	}	
		int afterDiv = interpreterCode.size();
		pushParameterDataOnStack("JUMP", afterDiv + 3);	
		pushParameterDataOnStack("LOAD", freeAddressPointer - 1);
		pushParameterDataOnStack("STORE", freeAddressPointer - 2);

}
