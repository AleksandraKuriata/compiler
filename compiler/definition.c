#include "definition.h"

const int notFound = -1;

int genericInitialize(char *identifier, int size, int isArrayOfVariables, int isInitializedInLoop){
	if (getPositionOfVariable(identifier) != notFound) {
		return notFound;
	}
	struct declaredVariable *initializedVariable;
	initializedVariable = (declaredVariable *) malloc(sizeof(declaredVariable));
	initializedVariable->identifier=(char *)malloc(sizeof(char)*strlen(identifier));
	strcpy(initializedVariable->identifier, identifier);

	initializedVariable->isArrayOfVariables = isArrayOfVariables;
	initializedVariable->isInitializedInLoop = isInitializedInLoop;
	initializedVariable->size = size;
	initializedVariable->address = variableFreeAddress;
	variableFreeAddress+=size;
	if(!isArrayOfVariables){
		initializedVariable->isLoopTemporaryVariable=0;
	} else if(isInitializedInLoop){
		initializedVariable->isLoopTemporaryVariable=1;
	}
	declaredVariables.push_back(initializedVariable);
	return 0;
}

/**
 *deklarowanej pojedynczej zmiennej
 */
int initializeVariable(char * identifier) {
		return genericInitialize(identifier, 1, 0, 0);
}
/**
 * deklarowanie tablicy
 */
int initializeArrayOfVariables(char * identifier, int size) {
		return genericInitialize(identifier, size, 1, 0);
}

/**
 * odnalezienie zmiennej po nazwie w wektorze ze zmiennymi
 */
int getPositionOfVariable(const char * identifier) {
	for (int i = 0; i < declaredVariables.size(); i++) {
		if (strcmp(declaredVariables[i]->identifier, identifier) == 0) {
			return i;
		}
	}
	return notFound;
}

/**
 * zadeklarowanie zmiennej uzywanej w petli for
 */
int initializeLoopVariable(char * identifier){
		return genericInitialize(identifier, 2, 0, 1);
}
/**
 * usuniecie zmiennej po nazwie
 */
int removeVariable(const char * identifier){
	int position=getPositionOfVariable(identifier);
	if(position<0) {
		return notFound;
	}
	declaredVariables.erase(declaredVariables.begin()+position);
	return 0;
}

