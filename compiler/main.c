    #include <stdio.h>
    #include <stdlib.h>
    #include <vector>
    #include <string.h>
    #include <iostream>
    #include "definition.h"
    #include "expressions.h"
    #include "parser.tab.h"

    extern FILE *yyin;
    extern int yylineno;
    int yylex();
    int yyerror(const char *);
    int printResultDataToFile(char* name);
    char* chopN(char* string);


int main(int argc, char *argv[])
{
	yyin = fopen( argv[1], "r" );
	return printResultDataToFile(argv[1]);
}

int printResultDataToFile(char* name){
    char *nameOfFile = chopN(name);
    FILE * result=fopen(nameOfFile,"w");
	yyparse();
	for(int i=0;i<interpreterCode.size();i++)
	{
		fprintf(result,"%s \n", interpreterCode[i].c_str());
	}
    return 0;
}

char* chopN(char* string)
{
    int length = strlen(string);
    string[length-3] = 'm';
    string[length-2] = 'r';
    string[length-1] = '\0';
    return string;
}