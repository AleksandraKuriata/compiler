#include "expressions.h"


//creating user input value
void createUserInputValue(long long value) {
	pushDataOnStack("ZERO");

	unsigned long position = 1;
	position = position << 63;

	//it can be SHL when was min. one time incremented.
	int incrementedCount = 0;
	for (int i = 0; i < 64; i++) {

		if (incrementedCount) {
			pushDataOnStack("SHL");
		}
		if (position & value) {	
			pushDataOnStack("INC");
			incrementedCount++;
		}
		// division the position by 2
		position = position >> 1;
	}

}

// function which take instructions for intepreter code and group it in correct order.
void pushDataOnStack(char * instruction) {
	sprintf(instructionBuffer, "%s", instruction);
	interpreterCode.push_back(instructionBuffer);
}
void pushParameterDataOnStack(char * instruction, int arg) {
	sprintf(instructionBuffer, "%s %d ", instruction, arg);
	interpreterCode.push_back(instructionBuffer);
}
