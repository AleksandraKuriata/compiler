/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_PARSER_TAB_H_INCLUDED
# define YY_YY_PARSER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 19 "compiler/parser.y" /* yacc.c:1909  */

	// Includes headers with code for process compilation
    #include "definition.h"
    #include "expressions.h"
	#include "src/activity.h"
	#include "src/conditions.h"

#line 52 "parser.tab.h" /* yacc.c:1909  */

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    PIDENTIFIER = 258,
    NUM = 259,
    VAR = 260,
    PROGRAM_BEGIN = 261,
    END = 262,
    SET = 263,
    IF = 264,
    ELSE = 265,
    THEN = 266,
    ENDIF = 267,
    WHILE = 268,
    DO = 269,
    ENDWHILE = 270,
    FOR = 271,
    FROM = 272,
    TO = 273,
    ENDFOR = 274,
    DOWNTO = 275,
    READ = 276,
    WRITE = 277,
    ADD = 278,
    SUB = 279,
    MULT = 280,
    DIV = 281,
    MOD = 282,
    OPERATOR_EQ = 283,
    OPERATOR_NE = 284,
    OPERATOR_LT = 285,
    OPERATOR_GT = 286,
    OPERATOR_LE = 287,
    OPERATOR_GE = 288,
    ARRAY_LBRACKET = 289,
    ARRAY_RBRACKET = 290,
    SEMICOLON = 291
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 27 "compiler/parser.y" /* yacc.c:1909  */
 
	// Defined current types for whole compiler grammar.
	char* text;
	char* num; 

#line 107 "parser.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_TAB_H_INCLUDED  */
