%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <vector>
    #include <string.h>
    #include <iostream>

	// This is pointer to the input compilation file
    extern FILE *yyin;
	// This is line which is current processing (parsed)
    extern int yylineno;
	// Implies the main entry point for lex, reads the input stream generates tokens, returns zero at the end of input stream . 
	// It is called to invoke the lexer
    int yylex();
	// The Bison parser expects to report the error by calling an error reporting function named yyerror, which you must supply
    int yyerror(const char *);
%}

%code requires {
	// Includes headers with code for process compilation
    #include "definition.h"
    #include "expressions.h"
	#include "src/activity.h"
	#include "src/conditions.h"
}

%union { 
	// Defined current types for whole compiler grammar.
	char* text;
	char* num; 
}

%{
	// push data on the top of the stack without any parameters
    void pushDataOnStack(const char * instruction);
	// push data on the top of the stack with parameter
	void pushParameterDataOnStack(const char * instruction, int arg);
	// change interpreter code to data with parameter
	void SwapDataWithArgument(const char * command,int number,int line);
	std::vector<std::string> interpreterCode;
	unsigned int freeAddressPointer=0;
	unsigned int variableFreeAddress=20;
	std::vector<int> jumpStack;
	char *instructionBuffer = new char[35];
	std::vector<int> staticAnalysisStack;
	std::vector<declaredVariable*> declaredVariables;
%}

%type<text> expression value


%token<text>   PIDENTIFIER
%token<num>    NUM

%token<text>   VAR
%token<text>   PROGRAM_BEGIN
%token<text>   END
%token<text>   SET

%token<text>   IF
%token<text>   ELSE
%token<text>   THEN
%token<text>   ENDIF
%token<text>   WHILE
%token<text>   DO
%token<text>   ENDWHILE
%token<text>   FOR
%token<text>   FROM
%token<text>   TO
%token<text>   ENDFOR
%token<text>   DOWNTO

%token<text>   READ
%token<text>   WRITE

%token<text>   ADD
%token<text>   SUB
%token<text>   MULT
%token<text>   DIV
%token<text>   MOD

%token<text>   OPERATOR_EQ
%token<text>   OPERATOR_NE
%token<text>   OPERATOR_LT
%token<text>   OPERATOR_GT
%token<text>   OPERATOR_LE
%token<text>   OPERATOR_GE

%token<text>   ARRAY_LBRACKET
%token<text>   ARRAY_RBRACKET
%token<text>   SEMICOLON
%%

program:
	VAR vdeclarations PROGRAM_BEGIN commands END
		{
			pushDataOnStack("HALT");
		};

vdeclarations:
	vdeclarations PIDENTIFIER
		{
			if(initializeVariable($2)<0)
			{
				sprintf(instructionBuffer,"Error line %d - variable redeclaration %s",yylineno,$2);
				fprintf(stderr, "%s \n",instructionBuffer);
		    		exit(0);
			}

		}
|	vdeclarations PIDENTIFIER ARRAY_LBRACKET NUM ARRAY_RBRACKET
		{
			initializeArrayOfVariables($2,atoll($4));
		}
|	%empty;

commands:
	commands command
	    {

	    }
|	command
		{

	    };

command:
	identifier
	    {
			int variableId=staticAnalysisStack.back();
			if(declaredVariables[variableId]->isLoopTemporaryVariable!=1)
			{
				staticAnalysisStack.pop_back();
		     		declaredVariables[variableId]->isInitializedInLoop = 1;
			}
			else
			{
				sprintf(instructionBuffer,"Error line %d - cannot change FOR variable %s",yylineno,declaredVariables[variableId]->identifier);
				fprintf(stderr, "%s \n",instructionBuffer);
				exit(0);
			}
        }
	SET expression SEMICOLON
	    {
			pushParameterDataOnStack("STOREI",freeAddressPointer-2);
			freeAddressPointer=freeAddressPointer-2;
	    }
|	IF condition THEN commands
        {
            int afterConditionLine=jumpStack.back();
            jumpStack.pop_back();
            jumpStack.push_back(interpreterCode.size());
            pushDataOnStack("LINE AFTER IF COMMANDS");
            SwapDataWithArgument("JZERO",interpreterCode.size(),afterConditionLine);
            freeAddressPointer--;
        }
	ELSE commands ENDIF
        {
            int beforeElseCommandLine=jumpStack.back();
            jumpStack.pop_back();
            SwapDataWithArgument("JUMP",interpreterCode.size(),beforeElseCommandLine);
        }
|	IF condition THEN commands ENDIF
        {
		    int afterConditionLine=jumpStack.back();
		    jumpStack.pop_back();
			SwapDataWithArgument("JZERO",interpreterCode.size(),afterConditionLine);
			freeAddressPointer--;
        }
|	WHILE
        {
        	jumpStack.push_back(interpreterCode.size());
        }
    condition DO commands ENDWHILE
        {
			int afterConditionLine=jumpStack.back();
			jumpStack.pop_back();
			int beforeConditionLine=jumpStack.back();
			jumpStack.pop_back();
			pushParameterDataOnStack("JUMP",beforeConditionLine);
			SwapDataWithArgument("JZERO",interpreterCode.size(),afterConditionLine);
			freeAddressPointer--;
        }
|	FOR PIDENTIFIER FROM value TO value DO
        {
			initializeLoopVariable($2);
		    int position=getPositionOfVariable($2);
		    int addressBegin=declaredVariables[position]->address;
		    createUserInputValue(addressBegin);
		    pushParameterDataOnStack("STORE",freeAddressPointer);
		    pushDataOnStack("INC");
		    pushParameterDataOnStack("STORE",freeAddressPointer+1);
		    pushParameterDataOnStack("LOAD",freeAddressPointer-2);
		    pushParameterDataOnStack("STOREI",freeAddressPointer);
		    pushParameterDataOnStack("LOAD",freeAddressPointer-1);
		    pushParameterDataOnStack("STOREI",freeAddressPointer+1);
		    jumpStack.push_back(interpreterCode.size());
			pushParameterDataOnStack("LOAD",addressBegin+1);
			pushDataOnStack("INC");
			pushParameterDataOnStack("SUB",addressBegin);
			jumpStack.push_back(interpreterCode.size());
			pushDataOnStack($2);
			freeAddressPointer= freeAddressPointer -2;
		}
    commands ENDFOR
        {
            int afterForToCondition=jumpStack.back();
            jumpStack.pop_back();
            int jumpLoopAddress=jumpStack.back();
            jumpStack.pop_back();
            std::string pid=interpreterCode[afterForToCondition];
            int position=getPositionOfVariable(pid.c_str());
            int addressBegin=declaredVariables[position]->address;
            pushParameterDataOnStack("LOAD",addressBegin);
            pushDataOnStack("INC");
            pushParameterDataOnStack("STORE",addressBegin);
            pushParameterDataOnStack("JUMP",jumpLoopAddress);
            SwapDataWithArgument("JZERO",interpreterCode.size(),afterForToCondition);
            removeVariable(declaredVariables[position]->identifier);
    	}
|	FOR PIDENTIFIER FROM value DOWNTO value
        {
        	initializeLoopVariable($2);
            int position=getPositionOfVariable($2);
            int addressBegin=declaredVariables[position]->address;
            createUserInputValue(addressBegin);
            pushParameterDataOnStack("STORE",freeAddressPointer);
            pushDataOnStack("INC");
            pushParameterDataOnStack("STORE",freeAddressPointer+1);
            pushParameterDataOnStack("LOAD",freeAddressPointer-2);
            pushDataOnStack("INC");
            pushParameterDataOnStack("STOREI",freeAddressPointer);
            pushParameterDataOnStack("LOAD",freeAddressPointer-1);
            pushDataOnStack("INC");
            pushParameterDataOnStack("STOREI",freeAddressPointer+1);
            freeAddressPointer=freeAddressPointer-2;
			jumpStack.push_back(interpreterCode.size());
			pushParameterDataOnStack("LOAD",addressBegin);
			pushDataOnStack("INC");
			pushParameterDataOnStack("SUB",addressBegin+1);
			jumpStack.push_back(interpreterCode.size());
			pushDataOnStack($2);
			pushParameterDataOnStack("LOAD",addressBegin);
            pushDataOnStack("DEC");
            pushParameterDataOnStack("STORE",addressBegin);
		}
    DO commands ENDFOR
        {
            int afterForToCondition=jumpStack.back();
            jumpStack.pop_back();
            int jumpLoopAddress=jumpStack.back();
            jumpStack.pop_back();
            std::string pid=interpreterCode[afterForToCondition];
            pushParameterDataOnStack("JUMP",jumpLoopAddress);
            SwapDataWithArgument("JZERO",interpreterCode.size(),afterForToCondition);
        }
|	READ identifier SEMICOLON
        {
			pushDataOnStack("GET");
            freeAddressPointer--;
            pushParameterDataOnStack("STOREI",freeAddressPointer);
			int pid=staticAnalysisStack.back();
            staticAnalysisStack.pop_back();
            if(declaredVariables[pid]->isLoopTemporaryVariable==1)
        		{
				sprintf(instructionBuffer,"Error line %d - cannot change FOR variable %s",yylineno,declaredVariables[pid]->identifier);
				fprintf(stderr, "%s \n",instructionBuffer);
				exit(0);
              	}
              	else
              	{
              	    declaredVariables[pid]->isInitializedInLoop = 1;
              	}
		}
|	WRITE value SEMICOLON
        {
            freeAddressPointer--;
            pushParameterDataOnStack("LOAD",freeAddressPointer);
            pushDataOnStack("PUT");
        }

expression:
	value
	    {
			pushParameterDataOnStack("LOAD",freeAddressPointer-1);
	    }
|	value ADD value
        {
        	addition();
			freeAddressPointer = freeAddressPointer-1;
        }
|	value SUB value
        {
        	subtraction();
			freeAddressPointer = freeAddressPointer-1;
        }
|	value MULT value
        {
            multiplication();
			freeAddressPointer = freeAddressPointer-1;
        }
|	value DIV value
        {
            division();
			freeAddressPointer = freeAddressPointer-1;
        }
|	value MOD value
        {
            modulo($3);
			freeAddressPointer = freeAddressPointer-1;
        }

condition:
	value OPERATOR_EQ value
		{
			conditionEqual();
			jumpStack.push_back(interpreterCode.size()-1);
		}
|	value OPERATOR_NE value
    	{
        	conditionNotEqual();
        	jumpStack.push_back(interpreterCode.size()-1);
		}
|	value OPERATOR_LT value
        {
            conditionLowerThen();
            jumpStack.push_back(interpreterCode.size()-1);
        }
|	value OPERATOR_GT value
        {
            conditionGreaterThen();
            jumpStack.push_back(interpreterCode.size()-1);
		}
|	value OPERATOR_LE value
        {
            conditionLowerEqualThen();
            jumpStack.push_back(interpreterCode.size()-1);
        }
|	value OPERATOR_GE value
        {
            conditionGreaterEqualThen();
            jumpStack.push_back(interpreterCode.size()-1);
        }

value:
	NUM
		{
			createUserInputValue(atoll($1));
			pushParameterDataOnStack("STORE",freeAddressPointer);
			freeAddressPointer++;
	    }
|	identifier
        {
            pushParameterDataOnStack("LOADI",freeAddressPointer-1);
            pushParameterDataOnStack("STORE",freeAddressPointer-1);
			int variableId=staticAnalysisStack.back();
			staticAnalysisStack.pop_back();
            if(declaredVariables[variableId]->isInitializedInLoop !=1)
				{
					sprintf(instructionBuffer,"Error line %d - variable not initialized %s",yylineno,declaredVariables[variableId]->identifier);
					fprintf(stderr, "%s \n",instructionBuffer);
					exit(0);
             	}
        }

identifier:
	PIDENTIFIER
	    {
			int pidIndex=getPositionOfVariable($1);
			if(pidIndex==-1)
				{
					sprintf(instructionBuffer,"Error line %d - variable not initialized %s",yylineno,$1);
					fprintf(stderr, "%s \n",instructionBuffer);
					exit(0);
				}
			else if(pidIndex>=0)
				{
					if(declaredVariables[pidIndex]->isArrayOfVariables==1)
						{
							sprintf(instructionBuffer,"Error line %d - wrong usage of array variable %s",yylineno,$1);
							fprintf(stderr, "%s \n",instructionBuffer);
							exit(0);
						}
					else
						{
							int address=declaredVariables[pidIndex]->address;
							createUserInputValue(address);
							pushParameterDataOnStack("STORE",freeAddressPointer);
							freeAddressPointer++;
							staticAnalysisStack.push_back(pidIndex);
						}
				}
	    }
|	PIDENTIFIER ARRAY_LBRACKET PIDENTIFIER ARRAY_RBRACKET
        {
            int pidArrayIndex=getPositionOfVariable($1);
            int pidElemIndex=getPositionOfVariable($3);
            if(pidArrayIndex==-1)
				{
					sprintf(instructionBuffer,"Error line %d - brak deklaracji zmiennej %s",yylineno,$1);
					fprintf(stderr, "%s \n",instructionBuffer);
					exit(0);
				}
			if(pidElemIndex==-1)
				{
					sprintf(instructionBuffer,"Error line %d - brak deklaracji zmiennej %s",yylineno,$3);
					fprintf(stderr, "%s \n",instructionBuffer);
					exit(0);
				}
            if(declaredVariables[pidArrayIndex]->isArrayOfVariables!=1)
             	{
					sprintf(instructionBuffer,"Error line %d - niewłaściwe użycie zmiennej %s",yylineno,$1);
					fprintf(stderr, "%s \n",instructionBuffer);
					exit(0);
             	}
            else
             	{
					int arrayAddressBegin=declaredVariables[pidArrayIndex]->address;
	             	createUserInputValue(arrayAddressBegin);
	             	int innerVarAddress=declaredVariables[pidElemIndex]->address;
	             	pushParameterDataOnStack("ADD",innerVarAddress);
	             	pushParameterDataOnStack("STORE",freeAddressPointer);
					freeAddressPointer++;
					staticAnalysisStack.push_back(pidArrayIndex);
             	}
        }
|	PIDENTIFIER ARRAY_LBRACKET NUM ARRAY_RBRACKET
        {
			int variableIndex=getPositionOfVariable($1);
			if(variableIndex==-1)
				{
					sprintf(instructionBuffer,"Error line %d - brak deklaracji zmiennej %s",yylineno,$1);
					fprintf(stderr, "%s \n",instructionBuffer);
					exit(0);
				}
			if(declaredVariables[variableIndex]->isArrayOfVariables!=1)
				{
					sprintf(instructionBuffer,"Error line %d - niewłaściwe użycie zmiennej %s",yylineno,$1);
					fprintf(stderr, "%s \n",instructionBuffer);
					exit(0);
				}
			else
				{
					int address=declaredVariables[variableIndex]->address;
					int innerValue=atol($3);
					address+=innerValue;
					createUserInputValue(address);
					pushParameterDataOnStack("STORE",freeAddressPointer);
					freeAddressPointer++;
					staticAnalysisStack.push_back(variableIndex);
				}
        }
%%

int yyerror(const char *error)
{
  sprintf(instructionBuffer, "Error: Line %d %s \n", yylineno,error);
  fprintf(stderr, "%s",instructionBuffer);
  exit(0);
}

void pushDataOnStack(const char * instruction) {
	sprintf(instructionBuffer, "%s", instruction);
	interpreterCode.push_back(instructionBuffer);
}


void pushParameterDataOnStack(const char * instruction, int arg) {
	sprintf(instructionBuffer, "%s %d ", instruction, arg);
	interpreterCode.push_back(instructionBuffer);
}

void SwapDataWithArgument(const char * command,int number,int line) {
	sprintf(instructionBuffer, "%s %d", command,number);
	interpreterCode[line]=instructionBuffer;
}